%global debug_package %{nil}
%global firmware_release 132

%global _firmwarepath  /usr/lib/firmware
%define _binaries_in_noarch_packages_terminate_build 0

Name:       linux-firmware-automotive
Version:    20220310
Release:    %{firmware_release}%{?dist}
Summary:    Firmware files of interest for automotive used by the Linux kernel
License:    GPL+ and GPLv2+ and MIT and Redistributable, no modification permitted
URL:        http://www.kernel.org/
BuildArch: noarch

Source0:    https://www.kernel.org/pub/linux/kernel/firmware/linux-firmware-%{version}.tar.xz
Patch1:     0001-Add-support-for-compressing-firmware-in-copy-firmwar.patch

BuildRequires:  make
Requires:   linux-firmware-whence
Provides:   kernel-firmware = %{version}
Obsoletes:  kernel-firmware < %{version}
Conflicts:  microcode_ctl < 2.1-0
Provides:   linux-firmware


%description
This package includes firmware files required for some devices to
operate.
This package is based on the linux-firmware package from Fedora but has been
edited to filter our most firmwares that aren't needed or of interest in the
automotive world.

%prep
%autosetup -p1 -n linux-firmware-%{version}

%build

%install
mkdir -p %{buildroot}/%{_firmwarepath}
mkdir -p %{buildroot}/%{_firmwarepath}/updates

make DESTDIR=%{buildroot}/ FIRMWAREDIR=%{_firmwarepath} installcompress

#Cleanup files we don't want to ship
pushd %{buildroot}/%{_firmwarepath}

# Drop all the files we do not want to install
for f in `find . -xtype f`;
do
  if [[ $f != *"Makefile"* \
        && $f != *"WHENCE"* \
        # the firmware we want for the rpi4
        && $f != *"brcmfmac43455"* \
        # firmwares we also need to keep, they are symlinked to
        && $f != *"cyfmac43455-sdio"* \
        && $f != *"copy-firmware.sh" ]]; then
    rm -f $f
  fi
done

# Drop the empty directories
for d in `find .  -type d |sort -r`;
do
    rmdir $d |true
done

# Remove executable bits from random firmware
find . -type f -executable -exec chmod -x {} \;
popd

# Create file list
FILEDIR=`pwd`
pushd %{buildroot}/%{_firmwarepath}
find . \! -type d > $FILEDIR/linux-firmware.files
find . -type d | sed -e '/^.$/d' > $FILEDIR/linux-firmware.dirs
popd
sed -i -e 's:^./::' linux-firmware.{files,dirs}
sed -i -e 's!^!/usr/lib/firmware/!' linux-firmware.{files,dirs}
sed -i -e 's/^/"/;s/$/"/' linux-firmware.files
sed -e 's/^/%%dir /' linux-firmware.dirs >> linux-firmware.files


%files -f linux-firmware.files
%dir %{_firmwarepath}
%license LICENCE.broadcom_bcm43xx GPL*


%changelog
* Thu Apr 14 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 20220310-132
- Add a Provides linux-firmware so it can replace it in our images

* Mon Apr 11 2022 Pierre-Yves Chibon <pingou@pingoured.fr> - 20220310-131
- Fork off from linux-firmware to create a rpi4 specific package
